import csv

class symbol(object):
    '''individual symbols + retrieve data'''

    def __init__(self, name):

        # symbol, csv, and file path
        self.name = name
        self.data = []
        self.labels = []
        self.csvPath = 'fh_20181116/full_history/' + self.name.upper() + '.csv'

        # check if symbol exists
        try:
            with open(self.csvPath, 'r') as csvFile:
                # print('[INFO]: SELECTED SYMBOL DATA FOUND')
                reader = csv.reader(csvFile)
                for rows in reader:
                    self.data.append(rows)

                # data labels from row 0
                # self.labels = self.data[0]

            # RETURNS
            # return True

        except FileNotFoundError:
            print('[ERROR]: SELECTED SYMBOL DATA NOT FOUND')

            # RETURNS
            # return False

    def getRecent(self):
        # first data row (most recent entry)
        recentVals = []
        recentVals.extend(self.data[1])

        # market attributes
        self.date = recentVals[0]
        self.volume = recentVals[1]
        self.open = recentVals[2]
        self.close = recentVals[3]
        self.high = recentVals[4]
        self.low = recentVals[5]
        self.adjclose = recentVals[6]

    def getOld(self, date):

        # check if data for day exists
        self.date = date

        # full stock history
        for i in range(len(self.data)):

            # if date matches and data isn't empty
            if date in self.data[i] and self.data[i][1] != '':

                # setting all market attributes and removing unnecessary data
                self.volume = float(self.data[i][1])
                self.open = float(self.data[i][2])
                self.close = float(self.data[i][3])
                self.high = float(self.data[i][4])
                self.low = float(self.data[i][5])
                self.adjclose = float(self.data[i][6])
                self.data = self.data[i]
                break

                

        # except ValueError:
        #     print('[ERROR] yaaa done goofed')
        # if self.date != self.data[1][0] and self.data[1][1] == self.volume:
        #     print('[ERROR] NO DATA FOR REQUESTED DATE')
        #
        #     # RETURNS
        #     # return False
        # else:
        #     pass
        #     # RETURNS
        #     # return True
