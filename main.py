'''
title: market query & analysis
author: zeeshan hooda
date: 2018-12-21
'''

import sys
from symbols import symbol

# start screen

# symbol data display
def dispData(s):
    print('DATE:   ', s.date)
    print('VOLUME: ', s.volume)
    print('OPEN:   ', s.open)
    print('CLOSE:  ', s.close)
    print('HIGH:   ', s.high)
    print('LOW:    ', s.low)
    print('ADJ:    ', s.adjclose)

def dispDelta(s):
    print('Average Volume: ', s.volume)
    print('Diff in Open:   ', s.open)
    print('Diff in Close:  ', s.close)
    print('Diff in High:   ', s.high)
    print('Diff in Low:    ', s.low)
    print('Diff in Adj Close:    ', s.adjclose)

# def verify(x):
    # if symbol(x)

# specific symbol menu
def symbolMenu():
    while True:
        print('[1]recent\n[2]specific date\n[3]change over time')
        choice = input('> ')


        selection = input('Symbol: ')

        if choice == '1':
            s1 = symbol(selection)
            s1.getRecent()
            dispData(s1)

        elif choice == '2':
            s2 = symbol(selection)
            s2.getOld(input('DESIRED DATE (YYYY-MM-DD): '))

            try:
                dispData(s2)
            except:
                print('No data for specific day')

        elif choice == '3':

            s1 = symbol(selection)
            s2 = symbol(selection)
            s3 = symbol(selection)

            s2.getOld(input('Start date (least recent) of desired period (YYYY-MM-DD): '))

            s1.getOld(input('End date (most recent) of desired period (YYYY-MM-DD): '))
            s3.getRecent()
            print(s2.date, s1.date)
            try:
                s3.volume = abs(float(s1.volume) + float(s2.volume))/2
                s3.open = float(s1.open) - float(s2.open)
                s3.close = float(s1.close) - float(s2.close)
                s3.high  = float(s1.high) - float(s2.high)
                s3.low  = float(s1.low) - float(s2.low)
                s3.adjclose = float(s1.adjclose) - float(s2.adjclose)
                dispDelta(s3)
            except AttributeError:
                print('One or more dates have missing data')



# input market or symbol

    # Symbol options + Graphs
        # current (open/close)
        # specific day
        # price delta over time

    # Index options
        #


# TESTING

symbolMenu()

# s1 = symbol(input('type symbol: '))
#
# s1.getRecent()
# print('date:', s1.date)
# print('open:', s1.open)
#
# s1.getOld('2000-01-01')
# print('date:', s1.date)
# print('open:', s1.open)
